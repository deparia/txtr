#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <errno.h>

#include <assert.h>
#ifdef RELEASE
#	undef assert
#	define assert(...)
#endif

#ifdef RELEASE
#	define ESL_NO_LOGGING
#endif
#define ESL_IMPLEMENTATION
#include "esl.h"

#define VERBOSE_LOGGING 0
#ifdef RELEASE
#	undef VERBOSE_LOGGING
#	define VERBOSE_LOGGING false
#endif
#if VERBOSE_LOGGING
#	define log_verbose(...) log_info(__VA_ARGS__)
#else
#	define log_verbose(...)
#endif

#define WIDTH 800
#define HEIGHT 600
#define TITLE "Text viewer"

#define BACKGROUND_COLOR 40, 40, 40, 255
#define CURSOR_COLOR 180, 180, 180, 200
#define FONT_SIZE 24
#define FONT_COLOR (SDL_Color){250, 250, 250, 255}
// NOTE: Has to be a monospace font.
#ifndef FONT_PATH
#	define FONT_PATH "resources/inconsolata.regular.ttf"
#endif

#define SCROLL_DISTANCE 48
#define MIN_CHAR_FOR_TEXTURE 256

#define SHOULD_LOG_DELTA 0
#ifdef RELEASE
#	undef SHOULD_LOG_DELTA
#	define SHOULD_LOG_DELTA false
#endif

typedef struct {
	const char *executable_name;

	SDL_Window *window;
	SDL_Renderer *renderer;

	int window_height;
	int window_width;

	TTF_Font *font;
	u32 font_size;
	u32 font_height; // Height of a single character (Only monospace is supported)
	u32 font_width; // Width of a single character (Only monospace is supported)

	const char *text_file_path;
} Config_t;

typedef struct {
	SDL_Texture *texture;
	SDL_Rect rect;
	bool should_render_this_texture;
} Texture_Ring_Item;

// WARNING: Causes an infinite loop if too low.
//          Just make sure its bigger than the number of lines visible
//          on the screen and you should be fine.
//          (At least until we add syntax highlighting)
#define TEXTURE_RING_LENGTH 256

// Ring buffer for textures.
typedef struct {
	Texture_Ring_Item *items;
	u32 *hashes;

	s32 top_index;
} Texture_Ring;

u32 get_hash(string s) {
	// Modified djb2 from https://www.cse.yorku.ca/~oz/hash.html
	u32 hash = 5381;

	for (u64 i = 0; i < s.length; ++i) {
		//         (hash * 33)      + chr
		hash = ((hash << 5) + hash) + (u32)s.data[i];
	}

	return hash;
}

void texture_ring_push(Texture_Ring *ring, u32 hash, Texture_Ring_Item item) {
	assert(ring != null);
	assert(ring->items != null);
	assert(ring->hashes != null);
	assert(item.texture != null);

	s32 i = ring->top_index;

	// WARNING: If the number of lines we need to render is >=
	//          TEXTURE_RING_LENGTH, this loop will never exit.
	//
	// FIXME: For some reason this loop never exits when
	//        there is no text visible on the screen
	while (ring->items[i].should_render_this_texture) {
		i = (i + 1) % TEXTURE_RING_LENGTH;
	}

	// @Minor: When walking backwards in the ringbuffer, the amount of textures
	//         that get destroyed is sometimes very uneven. This is because the
	//         textures get overwritten by the next one getting created.
	//         This is only really a problem when the number of lines visible
	//         on the screen gets close to TEXTURE_RING_LENGTH, which it should
	//         not.

	if (ring->items[i].texture != null) {
		SDL_DestroyTexture(ring->items[i].texture);
	}

	ring->hashes[i] = hash;
	ring->items[i] = item;
	ring->top_index = (i + 1) % TEXTURE_RING_LENGTH;
}

s32 texture_ring_find_unused(Texture_Ring *ring, u32 hash) {
	assert(ring != null);
	assert(ring->items != null);
	assert(ring->hashes != null);

	for (s32 i = 0; i < TEXTURE_RING_LENGTH; ++i) {
		if (ring->items[i].should_render_this_texture) {
			continue;
		}

		if (ring->hashes[i] == hash) {
			return i;
		}
	}

	return -1;
}

// A Line is just a String_Builder with some stuff on it.
typedef struct {
	u8 *data;
	u64 length;
	u64 capacity;

	bool do_not_free;
} Line;

static inline
void line_destroy(Line *l) {
	if (!l->do_not_free) {
		free(l->data);
	}

	l->data = null;
	l->length = 0;
	l->capacity = 0;
}

static inline
void line_expect_capacity(Line *line, u64 expected_capacity) {
	if (line->capacity >= expected_capacity) {
		return;
	}

	u64 next_capacity = ALIGN_TO_BYTES(line->capacity, 8) - 8;
	if (next_capacity == 0) {
		next_capacity = 8;
	}
	while (next_capacity < expected_capacity) next_capacity *= 2;

	if (line->do_not_free) {
		u8 *old_data = line->data;
		line->data = allocate(next_capacity);
		memcpy(line->data, old_data, line->length);
	} else {
		line->data = reallocate(line->data, next_capacity);
	}
	line->capacity = expected_capacity;
}

static inline
void line_expect_available(Line *line, u64 expected_available_bytes) {
	line_expect_capacity(line, line->length + expected_available_bytes);
}

define_array_type(Line_Array, Line);
define_array_functions(Line_Array, line_array);

typedef struct {
	// @Performance: Chunking system such that editing huge files becomes enjoyable.
	Line_Array text_array;
	String_Builder text_cache;

	Texture_Ring texture_ring;

	int scroll_x;
	int scroll_y;

	u64 cursor_line;
	u64 cursor_column;

	// NOTE: neither of these represent a length.
	u64 cursor_max_line;
	u64 cursor_max_column;

	u64 cursor_column_before_line_changed;

	u64 cursor_line_before_visual;
	u64 cursor_column_before_visual;

	enum {
		// Terminology from vi, may change
		MODE_NORMAL = 0,
		MODE_INSERT,
		MODE_VISUAL,
	} mode;

	// For when a command is waiting for input
	enum {
		COMMAND_NOT_SET = 0,
		COMMAND_TO,
		COMMAND_DELETE,
	} command;

	// TODO: Bool packing
	//       Does not really matter when we can only have 1 file open anyway.
	bool do_command_backwards;

	bool space_was_pressed;
	bool waiting_for_input;
	bool ignore_next_text_input_event;

	bool should_redraw_screen;

	bool should_quit;
} State_t;

static inline
Line line_new(string str) {
	Line line = {0};

	line.data = allocate(str.length);
	line.capacity = str.length;

	memcpy(line.data, str.data, str.length);
	line.length = str.length;

	line.do_not_free = false;

	return line;
}

static inline
void line_append(Line *line, string str) {
	line_expect_available(line, str.length);
	memcpy(line->data + line->length, str.data, str.length);
	line->length += str.length;
}

static inline
void line_remove_byte_index(Line *line, u64 index) {
	move_bytes(line->data + index, line->data + index + 1, line->length - index);
	line->length -= 1;
}

static inline
void line_insert_cstr(Line *line, const char *cstr, u64 index) {
	u64 cstr_length = length_of_cstr(cstr);
	line_expect_available(line, cstr_length);
	move_bytes(line->data + index + cstr_length, line->data + index, line->length - index);
	memcpy(line->data + index, cstr, cstr_length);
	line->length += cstr_length;
}

typedef struct {
	u64 starting_line;
	u64 starting_column;

	u64 ending_line;
	u64 ending_column;

	// I hate this
	bool only_use_lines;
} Text_Object;

bool setup_SDL(Config_t *config) {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
		log_error("Could not initialize SDL2.");
		return false;
	}

	if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE, &config->window, &config->renderer) < 0) {
		log_error("Could not create SDL2 window and renderer.");
		return false;
	}

	if (!config->window) {
		log_error("Could not create SDL2 window.");
		return false;
	}

	if (!config->renderer) {
		log_error("Could not create SDL2 renderer.");
		return false;
	}

	config->window_width  = WIDTH;
	config->window_height = HEIGHT;

	// Ignoring error, don't care if it could not set the title.
	SDL_SetWindowTitle(config->window, TITLE);

	SDL_SetRenderDrawBlendMode(config->renderer, SDL_BLENDMODE_BLEND);

	if (TTF_Init() < 0) {
		log_error("Could not initialize SDL_TTF");
		return false;
	}

	return true;
}

void reload_text_textures(Config_t *config, State_t *state) {
	log_verbose("Reloading text textures.");
	if (config->window_height == 0) {
		log_error("Window height is 0. Skipping reload.");
		return;
	}

	state->should_redraw_screen = true;

	for (u64 i = 0; i < TEXTURE_RING_LENGTH; ++i) {
		state->texture_ring.items[i].should_render_this_texture = false;
	}

	int font_height = 0;
	{ // Get the height of the surfaces.
		// @Font: Support for non-monospace fonts.
		SDL_Surface *surface = TTF_RenderText_Blended(config->font, ".", (SDL_Color){250, 250, 250, 255});
		font_height = surface->h;
		config->font_height = font_height;
		config->font_width = surface->w;
		SDL_FreeSurface(surface);
	}
	log_verbose("  Size of a single character is %u by %u pixels.", config->font_width, font_height);

	int window_top = -1 * state->scroll_y;

	u64 beginning_index;
	assert(font_height > 0);
	// Underflow protection
	// Wanted to put a sex joke about remembering to use protection here, but
	// couldn't think of a good one.
	if (window_top / font_height - 1 < 0) {
		beginning_index = 0;
	} else {
		beginning_index = window_top / font_height - 1;
	}

	u64 ending_index = (window_top + config->window_height) / font_height + 1;
	if (ending_index > state->text_array.length) {
		ending_index = state->text_array.length;
	}

	log_verbose("  Iterating text array from line %lu to %lu.", beginning_index, ending_index);
	assert(config->font_width > 0);
	for (u64 i = beginning_index; i < ending_index; ++i) {
		if (state->text_array.data[i].length == 0) {
			continue;
		}

		u64 line_y = i * font_height;
		string line = {
			.data = state->text_array.data[i].data,
			.length = state->text_array.data[i].length,
		};

		// Do we use the entire line for rendering or just the characters
		// visible on the screen?
		bool use_entire_line = line.length < MIN_CHAR_FOR_TEXTURE;

		u64 first_character_index = 0;
		u64 last_character_index;
		if (!use_entire_line) {
			if ((-1 * state->scroll_x) <= 0) {
				first_character_index = 0;
			} else {
				first_character_index = (-1 * state->scroll_x) / config->font_width;
			}

			if (first_character_index > line.length - 1) {
				continue;
			}

			last_character_index = (-1 * state->scroll_x + config->window_width) / config->font_width;

			if (last_character_index > line.length - 1) {
				last_character_index = line.length - 1;
			}

			assert(first_character_index <= last_character_index);

			line.data += first_character_index;
			line.length = (last_character_index - first_character_index) + 1;
			// @Minor: Weird stuff happens on large lines. (> 100mb)
		}

		u32 hash = get_hash(line);
		s32 index_in_ring = texture_ring_find_unused(&state->texture_ring, hash);
		if (index_in_ring >= 0) {
			// No need to update width and height.
			if (use_entire_line) {
				state->texture_ring.items[index_in_ring].rect.x = 0;
			} else {
				state->texture_ring.items[index_in_ring].rect.x = first_character_index * config->font_width;
			}
			state->texture_ring.items[index_in_ring].rect.y = line_y;
			state->texture_ring.items[index_in_ring].should_render_this_texture = true;
		} else {
			// TODO: Swap out characters for other characters
			//       Eg.
			//           '\t' -> 4 spaces or similar
			//           '\0' -> Some character that is not 0
			char buffer[line.length + 1];
			memcpy(buffer, line.data, line.length);
			buffer[line.length] = '\0';

			SDL_Surface *surface = TTF_RenderText_Blended(config->font, buffer, FONT_COLOR);

#ifndef RELEASE
			if (surface == null) {
				log_error("Surface was null! Error from SDL2: %s", SDL_GetError());
			}
#endif

			Texture_Ring_Item item = {
				.texture = SDL_CreateTextureFromSurface(config->renderer, surface),
				.rect = (SDL_Rect) {
					.x = use_entire_line ? 0 : first_character_index * config->font_width,
					.y = line_y,
					.w = surface->w,
					.h = surface->h,
				},
				.should_render_this_texture = true,
			};

#ifndef RELEASE
			if (item.texture == null) {
				log_error("Texture was null! Error from SDL2: %s", SDL_GetError());
			}
#endif

			SDL_FreeSurface(surface);

			// @Performance: Identical lines do not use the same texture.
			texture_ring_push(&state->texture_ring, hash, item);
		}
	}
	log_verbose("Finished reloading text textures.");
}

void reload_file_text(const Config_t *config, State_t *state) {
	log_verbose("Loading file '%s'.", config->text_file_path);

	state->text_cache.length = 0;

	if (!read_entire_file(config->text_file_path, &state->text_cache)) {
		if (errno != 0) {
			if (errno == ENOENT) {
				// @StatusMessage: This message should appear somewhere in the editor, not on stdout.
				printf("File '%s' does not exist. Opening anyway.\n", config->text_file_path);
			} else {
				fprintf(stderr, "ERROR: Could not read file '%s': %s.\n", config->text_file_path, strerror(errno));
				// TODO: We shouldn't exit when we can't read the file
				exit(1);
			}
		} else {
			fprintf(stderr, "ERROR: Could not read file '%s'.\n", config->text_file_path);
			// TODO: We shouldn't exit when we can't read the file
			exit(1);
		}
	}

	log_verbose("Finished reading file contents.");

	for (u64 i = 0; i < state->text_array.length; ++i) {
		line_destroy(&state->text_array.data[i]);
	}
	state->text_array.length = 0;

	if (state->text_cache.data != null) {
		log_verbose("Splitting file contents (%lu bytes) into lines.", state->text_cache.length);

		for (u64 i = 0; i < state->text_cache.length; ++i) {
			u8 *start = state->text_cache.data + i;

			u8 *next_newline = memchr(start, '\n', state->text_cache.length - i);
			if (next_newline == null) {
				// No more newlines in file
				next_newline = state->text_cache.data + state->text_cache.length - 1;
			}

			i += next_newline - start;

			Line line = {
				.data = start,
				.length = next_newline - start,
				.capacity = next_newline - start,

				.do_not_free = true,
			};

			line_array_append(&state->text_array, line);

#if VERBOSE_LOGGING
			if (state->text_array.length % (1000*1000) == 0) {
				log_verbose("Passed line %luM.", state->text_array.length / (1000*1000));
			}
#endif
		}

		log_verbose("Finished splitting file contents into %lu lines.", state->text_array.length);
	} else {
		line_array_append(&state->text_array, (Line){0});
	}
}

void save_file_text(const Config_t *config, State_t *state) {
	String_Builder text = {0};
	for (u64 i = 0; i < state->text_array.length; ++i) {
		string line = {
			.data = state->text_array.data[i].data,
			.length = state->text_array.data[i].length,
		};
		string_builder_append(&text, line);
		string_builder_append_byte(&text, '\n');
	}

	if (!write_entire_file(config->text_file_path, text)) {
		if (errno != 0) {
			fprintf(stderr, "ERROR: Could not write to file '%s': %s.\n", config->text_file_path, strerror(errno));
		} else {
			fprintf(stderr, "ERROR: Could not write to file '%s'.\n", config->text_file_path);
		}
		// TODO: We shouldn't exit when we can't write the file
		exit(1);
	}
}

void update_cursor_info(State_t *state) {
	state->cursor_max_line = state->text_array.length - 1;
	state->cursor_max_column = state->text_array.data[state->cursor_line].length;
}

void move_cursor_by(State_t *state, s32 line_count, s32 column_count) {
	u64 line = state->cursor_line;
	u64 column = state->cursor_column;

	// Sets cursor to 0 on underflow, thereby snapping it to the beginning.
	if (line_count <= 0 && line + line_count > line) {
		line = 0;
	} else if (line_count >= 0 && line + line_count < line) {
		line = U64_MAX;
		log_info("Cursor position overflow detected and fixed.");
	} else {
		if (line + line_count >= state->text_array.length - 1) {
			line = state->text_array.length - 1;
		} else {
			line += line_count;
			if (column_count == 0) {
				column = state->cursor_column_before_line_changed;
			}
		}
	}

	// Sets cursor to 0 on underflow, thereby snapping it to the beginning.
	if (column_count <= 0 && column + column_count > column) {
		column = 0;
	} else if (column_count >= 0 && column + column_count < column) {
		column = U64_MAX;
		log_info("Cursor position overflow detected and fixed.");
	} else {
		if (column + column_count > state->text_array.data[line].length) {
			column = state->text_array.data[line].length;
		} else {
			column += column_count;
			state->cursor_column_before_line_changed = column;
		}
	}

	state->cursor_line = line;
	state->cursor_column = column;
	update_cursor_info(state);
}

// 'weight' is the weight to apply to the screen position relative to the cursor, where:
//     0.0 means put the screen in a position such that the cursor is at the top of the screen.
//     0.5 means put the screen in a position such that the cursor is at the center of the screen.
//     1.0 means put the screen in a position such that the cursor is at the bottom of the screen.
//     Anything inbetween is also supported.
//  'snap' snaps the cursor to closest border. Only supported on weight == 1.0 or 0.0.
void set_screen_at_position_relative_to_cursor_y(const Config_t *config, State_t *state, float weight, bool snap) {
	// @Cleanup: This kind of works, but is hella jank. I don't like how the user
	// must specify whether or not to snap to a border.

	state->scroll_y = (config->font_height *- state->cursor_line) - (config->font_height / 2) + (u64)((float)config->window_height * weight);
	if (snap) {
		if (weight == 0.0) {
			state->scroll_y += config->font_height / 2;
		} else if (weight == 1.0) {
			state->scroll_y -= config->font_height / 2;
		} else {
			log_warning("%s: Snapping is not supported on a weight of %f.", __func__, weight);
		}
	}
}

void set_screen_at_position_relative_to_cursor_x(const Config_t *config, State_t *state, float weight, bool snap) {
	// @Cleanup: Jank function. See comment in set_screen_at_position_relative_to_cursor_y().

	state->scroll_x = (config->font_width *- state->cursor_column) - (config->font_width / 2) + (u64)((float)config->window_width * weight);
	if (snap) {
		if (weight == 0.0) {
			state->scroll_x += config->font_width / 2;
		} else if (weight == 1.0) {
			state->scroll_x -= config->font_width / 2;
		} else {
			log_warning("%s: Snapping is not supported on a weight of %f.", __func__, weight);
		}
	}
}

void maybe_scroll_to_cursor_y(const Config_t *config, State_t *state) {
	if ((s64)state->cursor_line * config->font_height <= -1 * state->scroll_y) {
		set_screen_at_position_relative_to_cursor_y(config, state, 0.0, true);
	} else if ((s64)state->cursor_line * config->font_height >= -1 * state->scroll_y + config->window_height) {
		set_screen_at_position_relative_to_cursor_y(config, state, 1.0, true);
	}
}

void maybe_scroll_to_cursor_x(const Config_t *config, State_t *state) {
	if ((s64)state->cursor_column * config->font_width <= -1 * state->scroll_x) {
		set_screen_at_position_relative_to_cursor_x(config, state, 0.0, true);
	} else if ((s64)state->cursor_column * config->font_width >= -1 * state->scroll_x + config->window_width) {
		set_screen_at_position_relative_to_cursor_x(config, state, 1.0, true);
	}
}

void scroll_to_cursor(const Config_t *config, State_t *state) {
	maybe_scroll_to_cursor_x(config, state);
	maybe_scroll_to_cursor_y(config, state);
}

bool is_whitespace(char c) {
	return c == ' ' || c == '\n' || c == '\t' || c == '\r';
}

void cursor_last_non_whitespace(State_t *state) {
	if (state->cursor_max_column == 0) {
		state->cursor_column = 0;
		state->cursor_column_before_line_changed = 0;
	} else {
		u64 i = state->cursor_max_column - 1;
		while (i > 0 && is_whitespace(state->text_array.data[state->cursor_line].data[i])) {
			i -= 1;
		}
		state->cursor_column = i;
		move_cursor_by(state, 0, 1);
	}
}

void cursor_first_non_whitespace(State_t *state) {
	if (state->cursor_max_column == 0) {
		state->cursor_column = 0;
		state->cursor_column_before_line_changed = 0;
	} else {
		u64 i = 0;
		while (i < state->cursor_max_column && is_whitespace(state->text_array.data[state->cursor_line].data[i])) {
			i += 1;
		}
		state->cursor_column = i;
		state->cursor_column_before_line_changed = i;
	}
}

// Returns false if there are no characters to remove.
bool remove_character_left(State_t *state) {
	if (state->text_array.data[state->cursor_line].length > 0 && state->cursor_column > 0) {
		line_remove_byte_index(&state->text_array.data[state->cursor_line], state->cursor_column - 1);
		move_cursor_by(state, 0, -1);
		return true;
	}
	return false;
}

// Returns false if there are no characters to remove.
bool remove_character_right(State_t *state) {
	if (state->text_array.data[state->cursor_line].length > 0 && state->cursor_column < state->text_array.data[state->cursor_line].length) {
		line_remove_byte_index(&state->text_array.data[state->cursor_line], state->cursor_column);
		return true;
	}
	return false;
}

void merge_line_with_above(State_t *state) {
	if (state->text_array.length > 1 && state->cursor_line > 0) {
		u64 previous_max = state->text_array.data[state->cursor_line - 1].length;

		string current_line = {
			.data = state->text_array.data[state->cursor_line].data,
			.length = state->text_array.data[state->cursor_line].length,
		};

		line_append(&state->text_array.data[state->cursor_line - 1], current_line);

		line_destroy(&state->text_array.data[state->cursor_line]);
		line_array_remove_ordered(&state->text_array, state->cursor_line);

		move_cursor_by(state, -1, 0);
		state->cursor_column = previous_max;
		state->cursor_column_before_line_changed = state->cursor_column;
	}
}

void cursor_previous_char_match_on_line(State_t *state, char c) {
	if (state->cursor_max_column == 0 || state->cursor_column == 0) {
		state->cursor_column_before_line_changed = state->cursor_column;
		return;
	}

	u64 i = state->cursor_column - 1;
	while (i > 0 && c != state->text_array.data[state->cursor_line].data[i]) {
		i -= 1;
	}
	if (i > 0) {
		state->cursor_column = i;
		state->cursor_column_before_line_changed = i;
	} else if (c == state->text_array.data[state->cursor_line].data[i]) {
		state->cursor_column = 0;
		state->cursor_column_before_line_changed = 0;
	}
}

void cursor_next_char_match_on_line(State_t *state, char c) {
	if (state->cursor_max_column == 0 || state->cursor_column >= state->cursor_max_column) {
		state->cursor_column_before_line_changed = state->cursor_column;
		return;
	}

	u64 i = state->cursor_column + 1;
	while (i < state->cursor_max_column && c != state->text_array.data[state->cursor_line].data[i]) {
		i += 1;
	}
	if (i != state->cursor_max_column) {
		state->cursor_column = i;
		state->cursor_column_before_line_changed = i;
	}
}

void new_line_below(State_t *state) {
	Line line = {0};
	line_array_insert(&state->text_array, line, state->cursor_line + 1);
	move_cursor_by(state, 1, 0);
}

void reload_and_update(Config_t *config, State_t *state) {
	reload_text_textures(config, state);
	update_cursor_info(state);
}

void cursor_snap(State_t *state) {
	update_cursor_info(state);
	if (state->cursor_column > state->cursor_max_column) {
		state->cursor_column = state->cursor_max_column;
		state->cursor_column_before_line_changed = state->cursor_column;
	}
	if (state->cursor_line >= state->text_array.length) {
		state->cursor_line = state->text_array.length - 1;
	}
}

bool get_next_text_object(Text_Object *object, State_t *state, char c) {
	assert(object != null);
	assert(state != null);

	if (c == '\0') {
		return false;
	}

	memset(object, 0, sizeof(Text_Object));

	switch (c) {
	case 'l': {
		object->only_use_lines = true;
		object->starting_line = state->cursor_line;
		object->ending_line = state->cursor_line;
		return true;
	} break;
	}

	return false;
}

// Inclusive; 2:4 will remove 3 lines (2, 3 and 4).
void remove_line_range(State_t *state, u64 start, u64 end) {
	assert(state != null);
	assert(start <= end);
	assert(0 < start);
	assert(end < state->text_array.length);

	if (state->text_array.length > 1) {
		for (u64 i = start; i <= end; ++i) {
			line_destroy(&state->text_array.data[i]);
		}
		line_array_remove_many(&state->text_array, start, end - start + 1);
	} else {
		state->text_array.data[0].length = 0;
	}
}

bool handle_multimode_bind(Config_t *config, State_t *state, SDL_Keycode keycode, SDL_Keymod modifiers) {
	if (state->mode == MODE_NORMAL || state->mode == MODE_VISUAL) {
		if (modifiers & KMOD_SHIFT || state->space_was_pressed) {
			switch (keycode) {
			case SDLK_s: {
				state->cursor_column = 0;
				state->cursor_column_before_line_changed = state->cursor_column;
				maybe_scroll_to_cursor_x(config, state);
				return true;
			} break;

			case SDLK_e: {
				cursor_last_non_whitespace(state);
				maybe_scroll_to_cursor_x(config, state);
				return true;
			} break;

			case SDLK_t: {
				state->waiting_for_input = true;
				state->command = COMMAND_TO;
				state->do_command_backwards = true;
				return true;
			} break;

			// TODO: Goto command for text objects
			case SDLK_g: {
				state->cursor_line = state->cursor_max_line;
				cursor_snap(state);
				scroll_to_cursor(config, state);
				return true;
			} break;

			default: break;
			}
		} else if (modifiers == 0) {
			switch (keycode) {
			case SDLK_q: {
				state->should_quit = true;
				return true;
			} break;

			case SDLK_h: {
				move_cursor_by(state, 0, -1);
				maybe_scroll_to_cursor_x(config, state);
				return true;
			} break;

			case SDLK_j: {
				move_cursor_by(state, 1, 0);
				scroll_to_cursor(config, state);
				return true;
			} break;

			case SDLK_k: {
				move_cursor_by(state, -1, 0);
				scroll_to_cursor(config, state);
				return true;
			} break;

			case SDLK_l: {
				move_cursor_by(state, 0, 1);
				maybe_scroll_to_cursor_x(config, state);
				return true;
			} break;

			case SDLK_s: {
				cursor_first_non_whitespace(state);
				maybe_scroll_to_cursor_x(config, state);
				return true;
			} break;

			case SDLK_e: {
				state->cursor_column = state->cursor_max_column;
				state->cursor_column_before_line_changed = state->cursor_column;
				maybe_scroll_to_cursor_x(config, state);
				return true;
			} break;

			case SDLK_t: {
				state->waiting_for_input = true;
				state->command = COMMAND_TO;
				return true;
			} break;

			// TODO: Goto command for text objects
			case SDLK_g: {
				state->cursor_line = 0;
				cursor_snap(state);
				scroll_to_cursor(config, state);
				return true;
			} break;

			default: break;
			}
		} else if (modifiers & KMOD_CTRL) {
			switch (keycode) {
			case SDLK_r: {
				// @StatusMessage
				printf("Reloading file '%s'.\n", config->text_file_path);
				reload_file_text(config, state);
				reload_and_update(config, state);
				cursor_first_non_whitespace(state);
				scroll_to_cursor(config, state);
				return true;
			} break;

			case SDLK_s: {
				// @StatusMessage
				printf("Saving file '%s' to disk.\n", config->text_file_path);
				save_file_text(config, state);
				return true;
			} break;

			case SDLK_t: {
				set_screen_at_position_relative_to_cursor_y(config, state, 0.0, true);
				return true;
			} break;

			case SDLK_g: {
				set_screen_at_position_relative_to_cursor_y(config, state, 0.5, false);
				return true;
			} break;

			case SDLK_b: {
				set_screen_at_position_relative_to_cursor_y(config, state, 1.0, true);
				return true;
			} break;

			case SDLK_y: {
				set_screen_at_position_relative_to_cursor_x(config, state, 0.0, true);
				return true;
			} break;

			case SDLK_u: {
				set_screen_at_position_relative_to_cursor_x(config, state, 0.5, false);
				return true;
			} break;

			case SDLK_i: {
				set_screen_at_position_relative_to_cursor_x(config, state, 1.0, true);
				return true;
			} break;

			default: break;
			}
		}
	}

	return false;
}

bool handle_insert_mode_bind(Config_t *config, State_t *state, SDL_Keycode keycode, SDL_Keymod modifiers) {
	if (modifiers & KMOD_SHIFT) {
		switch (keycode) {
		case SDLK_BACKSPACE: {
			if (!remove_character_left(state)) {
				merge_line_with_above(state);
				scroll_to_cursor(config, state);
			}
			reload_and_update(config, state);
			// SDL2 does not send a textinput event after backspace
			return false;
		} break;

		default: break;
		}
	} else if (modifiers == 0) {
		switch (keycode) {
		case SDLK_ESCAPE: {
			state->mode = MODE_NORMAL;
			return true;
		} break;

		case SDLK_BACKSPACE: {
			if (!remove_character_left(state)) {
				merge_line_with_above(state);
				scroll_to_cursor(config, state);
			}
			reload_and_update(config, state);
			// SDL2 does not send a textinput event after backspace
			return false;
		} break;

		case SDLK_RETURN: {
			new_line_below(state);
			scroll_to_cursor(config, state);
			reload_and_update(config, state);
			// We return false because SDL2 does not send a textinput event
			// after pressing return
			return false;
		} break;

		default: break;
		}
	}

	return false;
}

bool handle_normal_mode_bind(Config_t *config, State_t *state, SDL_Keycode keycode, SDL_Keymod modifiers) {
	if (modifiers & KMOD_SHIFT || state->space_was_pressed) {
		switch (keycode) {
		case SDLK_x: {
			remove_character_left(state);
			reload_and_update(config, state);
			return true;
		} break;

		case SDLK_d: {
			state->waiting_for_input = true;
			state->command = COMMAND_DELETE;
			state->do_command_backwards = true;
			return true;
		} break;

		default: break;
		}
	} else if (modifiers == 0) {
		switch (keycode) {
		case SDLK_i: {
			state->mode = MODE_INSERT;
			return true;
		} break;

		case SDLK_x: {
			remove_character_right(state);
			reload_and_update(config, state);
			return true;
		} break;

		case SDLK_d: {
			state->waiting_for_input = true;
			state->command = COMMAND_DELETE;
			return true;
		} break;

		case SDLK_o: {
			new_line_below(state);
			scroll_to_cursor(config, state);
			reload_and_update(config, state);
			state->mode = MODE_INSERT;
			return true;
		} break;

		case SDLK_v: {
			state->cursor_line_before_visual = state->cursor_line;
			state->cursor_column_before_visual = state->cursor_column;
			state->mode = MODE_VISUAL;
			return true;
		} break;

		default: break;
		}
	}

	return false;
}

bool handle_visual_mode_bind(Config_t *config, State_t *state, SDL_Keycode keycode, SDL_Keymod modifiers) {
	(void) config;
	if (modifiers == 0) {
		switch (keycode) {
		case SDLK_ESCAPE: {
			state->mode = MODE_NORMAL;
			return true;
		} break;

		default: break;
		}
	}

	return false;
}

bool handle_keybind(Config_t *config, State_t *state, SDL_Keysym keysym) {
	SDL_Keycode keycode  = keysym.sym;
	SDL_Keymod modifiers = keysym.mod;

	log_verbose("  Handling keybind for keycode %i.", keycode);

	// Some bindings, like moving the cursor, are the same across multiple modes.
	bool keybind_handled = handle_multimode_bind(config, state, keycode, modifiers);

	if (!keybind_handled) {
		if (state->mode == MODE_INSERT) {
			keybind_handled = handle_insert_mode_bind(config, state, keycode, modifiers);
		} else if (state->mode == MODE_NORMAL) {
			keybind_handled = handle_normal_mode_bind(config, state, keycode, modifiers);
		} else if (state->mode == MODE_VISUAL) {
			keybind_handled = handle_visual_mode_bind(config, state, keycode, modifiers);
		}
	}

	state->space_was_pressed = false;

	if (!keybind_handled && (state->mode == MODE_NORMAL || state->mode == MODE_VISUAL)) {
		if (keycode == SDLK_SPACE) {
			state->space_was_pressed = true;
			keybind_handled = true;
		}
	}

	return keybind_handled;
}

// @Performance: If the selection area gets very large, the performance suffers.
//               This is because we draw everything in the selection that is
//               offscreen too.
void draw_selection(const Config_t *config, const State_t *state, Text_Object object) {
	if (object.only_use_lines) {
		// TODO
	} else {
		if (object.ending_line == object.starting_line) {
			SDL_Rect rect = {
				.x = object.starting_column * config->font_width  + state->scroll_x,
				.y = object.starting_line   * config->font_height + state->scroll_y,
				.w = (object.ending_column - object.starting_column) * config->font_width,
				.h = config->font_height,
			};

			SDL_SetRenderDrawColor(config->renderer, CURSOR_COLOR);
			SDL_RenderFillRect(config->renderer, &rect);
		} else {
			SDL_SetRenderDrawColor(config->renderer, CURSOR_COLOR);

			// First line
			SDL_Rect rect = {
				.x = object.starting_column * config->font_width  + state->scroll_x,
				.y = object.starting_line   * config->font_height + state->scroll_y,
				.w = (state->text_array.data[object.starting_line].length - object.starting_column) * config->font_width,
				.h = config->font_height,
			};
			SDL_RenderFillRect(config->renderer, &rect);

			{
				// Middle lines, rect gets prepped
				rect = (SDL_Rect) {
					.x = state->scroll_x,
					.y = object.starting_line * config->font_height + state->scroll_y,
					.w = 0,
					.h = config->font_height,
				};
				Line *array = state->text_array.data;
				// TODO: Maybe we should just advance the pointer instead of this index party
				for (u64 i = 1; i + object.starting_line < object.ending_line; ++i) {
					rect.y += config->font_height;
					rect.w = array[i + object.starting_line].length * config->font_width;

					SDL_RenderFillRect(config->renderer, &rect);
				}
			}
			// Last line
			rect = (SDL_Rect) {
				.x = state->scroll_x,
				.y = object.ending_line   * config->font_height + state->scroll_y,
				.w = object.ending_column * config->font_width,
				.h = config->font_height,
			};
			SDL_RenderFillRect(config->renderer, &rect);
		}
	}
}

void handle_events(Config_t *config, State_t *state) {
	SDL_Event event;
	bool should_reload = false;

#if VERBOSE_LOGGING
	bool did_handle_events = false;
#endif


	while (SDL_PollEvent(&event)) {
#if VERBOSE_LOGGING
		if (!did_handle_events) {
			log_verbose("Handling SDL2 events.");
			did_handle_events = true;
		}
#endif
		switch (event.type) {
			case SDL_QUIT: {
				state->should_quit = true;
			} break;

			case SDL_KEYDOWN: {
				if (!state->waiting_for_input) {
					if (handle_keybind(config, state, event.key.keysym)) {
						state->ignore_next_text_input_event = true;
						// @Performace: Not every keypress needs 
						//              a reload afterwards.
						should_reload = true;
					}
				} else {
					if (event.key.keysym.sym == SDLK_ESCAPE) {
						state->waiting_for_input = false;
					}
				}

				state->should_redraw_screen = true;
			} break;

			case SDL_TEXTINPUT: {
				if (state->ignore_next_text_input_event) {
					state->ignore_next_text_input_event = false;
					break;
				}

				if (state->waiting_for_input) {
					switch (state->command) {
					case COMMAND_NOT_SET: {
						log_error("Command not set even though we are waiting for input.");
					} break;

					case COMMAND_TO: {
						if (state->do_command_backwards) {
							cursor_previous_char_match_on_line(state, *event.text.text);
						} else {
							cursor_next_char_match_on_line(state, *event.text.text);
						}
					} break;
					case COMMAND_DELETE: {
						if (state->do_command_backwards) {
							// TODO: delete backwards
							//get_previous_text_object(state, *event.text.text);
						} else {
							Text_Object object;
							if (get_next_text_object(&object, state, *event.text.text)) {
								if (object.only_use_lines) {
									remove_line_range(state, object.starting_line, object.ending_line);
									update_cursor_info(state);

									cursor_snap(state);
								} else {
									log_info("TODO: Accounting for columns.");
								}
							}
						}
						update_cursor_info(state);
						should_reload = true;
					} break;
					}

					state->command = COMMAND_NOT_SET;
					state->waiting_for_input = false;
					state->do_command_backwards = false;
				} else {
					if (state->mode == MODE_INSERT) {
						line_insert_cstr(&state->text_array.data[state->cursor_line], event.text.text, state->cursor_column);
						move_cursor_by(state, 0, length_of_cstr(event.text.text));
						update_cursor_info(state);
						should_reload = true;
					}
				}
				scroll_to_cursor(config, state);
			} break;

			case SDL_WINDOWEVENT: {
				// TODO: This just redraws the screen anytime we receive a
				//       window event. Maybe some window events need special
				//       treatment?
				state->should_redraw_screen = true;

				if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
					u64 w = event.window.data1;
					u64 h = event.window.data2;

					// Hardcoded minimum window size
					if (w > 10 && h > 10) {
						config->window_width = w;
						config->window_height = h;
					}

					should_reload = true;
				}
			} break;

			case SDL_MOUSEWHEEL: {
				state->scroll_y += event.wheel.y * SCROLL_DISTANCE;
				should_reload = true;
				log_verbose("Handled mousewheel movement.");
			} break;
		}
	}

#if VERBOSE_LOGGING
	if (did_handle_events) {
		log_verbose("Finished handling events.");
	}
#endif

	if (should_reload && !state->should_quit) reload_text_textures(config, state);
}

void draw(Config_t *config, State_t *state) {
	// Background
	SDL_SetRenderDrawColor(config->renderer, BACKGROUND_COLOR);
	SDL_RenderClear(config->renderer);

	for (u64 i = 0; i < TEXTURE_RING_LENGTH; ++i) {
		if (state->texture_ring.items[i].should_render_this_texture) {
			SDL_Rect rect = state->texture_ring.items[i].rect;
			rect.x += state->scroll_x;
			rect.y += state->scroll_y;
			SDL_RenderCopy(config->renderer, state->texture_ring.items[i].texture, null, &rect);
		}
	}

	{ // Cursor
		SDL_Rect rect = {
			.x = state->cursor_column * config->font_width  + state->scroll_x,
			.y = state->cursor_line   * config->font_height + state->scroll_y,
			.w = config->font_width,
			.h = config->font_height,
		};

		if (state->mode == MODE_INSERT) {
			rect.w /= 3;
		} else if (state->mode == MODE_VISUAL) {
			rect.w /= 2;
		}

		SDL_SetRenderDrawColor(config->renderer, CURSOR_COLOR);
		SDL_RenderFillRect(config->renderer, &rect);
	}

	if (state->mode == MODE_VISUAL &&
			!(state->cursor_column_before_visual == state->cursor_column
				&& state->cursor_line_before_visual == state->cursor_line)) {
		Text_Object object = {
			.starting_line = state->cursor_line_before_visual,
			.starting_column = state->cursor_column_before_visual,

			.ending_line = state->cursor_line,
			.ending_column = state->cursor_column,
		};
		// TODO: swap .starting and .ending if .ending is before .starting
		draw_selection(config, state, object);
	}

	SDL_RenderPresent(config->renderer);
}

int main(int argc, char **argv) {
	if (argc <= 1) {
		// TODO: open empty file with no filename
		fprintf(stderr, "No file provided. Aborting...\n");
		return 1;
	}

	Config_t config = {0};
	config.executable_name = argv[0];
	config.text_file_path = argv[1];

	State_t state = {0};

	state.texture_ring.hashes = allocate(TEXTURE_RING_LENGTH *
	                                      sizeof(*state.texture_ring.hashes));
	memset(state.texture_ring.hashes, 0,
	       TEXTURE_RING_LENGTH * sizeof(*state.texture_ring.hashes));

	state.texture_ring.items = allocate(TEXTURE_RING_LENGTH *
	                                    sizeof(Texture_Ring_Item));
	memset(state.texture_ring.items, 0,
	       TEXTURE_RING_LENGTH * sizeof(Texture_Ring_Item));

	reload_file_text(&config, &state);

	if (!setup_SDL(&config)) {
		return 1;
	}

	log_verbose("Successfully set up SDL2.");

	config.font_size = FONT_SIZE;
    config.font = TTF_OpenFont(FONT_PATH, config.font_size);
    if (config.font == null) {
		log_error("Could not open font '%s'.", FONT_PATH);
		return 1;
    }

	log_verbose("Loaded and set font from '%s'.", FONT_PATH);

#if SHOULD_LOG_DELTA
	Uint64 time_previous;
	Uint64 time_now = SDL_GetPerformanceCounter();
#endif

	log_verbose("Beginning main loop.");
	while (!state.should_quit) {
#if SHOULD_LOG_DELTA
		time_previous = time_now;
		time_now = SDL_GetPerformanceCounter();
#endif

		update_cursor_info(&state);

		handle_events(&config, &state);

		if (state.should_redraw_screen && !state.should_quit) {
			draw(&config, &state);
			state.should_redraw_screen = false;
		}

#if SHOULD_LOG_DELTA
		double time_delta = (time_now - time_previous)*1000 / (double)SDL_GetPerformanceFrequency();
		log_info("DELTA: %lf", time_delta);
#endif

		// TODO
		SDL_Delay(16);
	}

	log_verbose("Destroying text textures.");
	for (u64 i = 0; i < TEXTURE_RING_LENGTH; ++i) {
		// NOTE: This leaves texture_ring in an unusable state.
		//       If you want to make the ring usable afterwards, just memset
		//       everything to 0 or something.
		if (state.texture_ring.items[i].texture != null) {
			SDL_DestroyTexture(state.texture_ring.items[i].texture);
		}
	}
	/* Yes, we intentionally avoid freeing these for better performance.
	 * The program is about to quit anyway, so why bother?
	 * Your operating system is the ultimate garbage collector.
	free(state.texture_ring.hashes);
	free(state.texture_ring.items);

	for (u64 i = 0; i < state.text_array.length; ++i) {
		line_destroy(&state.text_array.data[i]);
	}
	line_array_destroy(&state.text_array);

	string_builder_destroy(&state.text_cache);
	*/

	log_verbose("Quitting SDL2_ttf.");
	TTF_CloseFont(config.font);
	TTF_Quit();

	log_verbose("Quitting SDL2.");
	SDL_DestroyRenderer(config.renderer);
	SDL_DestroyWindow(config.window);
	SDL_Quit();

	log_verbose("Done.");
}

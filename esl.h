//
// Single-header library for easing the development of C programs.
// Makes extensive use of C's standard library; some headers will get included.
// Assumes the user builds their program around it.
//
// This header is licensed under the MIT license. See bottom for more.
//
// NOTE:
//   - Everything included from the standard library is marked with [[STDLIB]].
//   - free() from the standard library gets renamed to mfree(); free()
//   is now supposed to be used with allocate(), not malloc().
//   - Some functions log errors directly to standard out. To prevent
//   this (eg. when compiling for release), define ESL_NO_LOGGING.
//   - By default logging does not include file and line number. To enable
//   this, define ESL_LOG_FILE_INFO.
//

#ifndef ESL_H_
#define ESL_H_

// [[STDLIB]]
#include <stdbool.h>
#include <stdlib.h>

// Define this yourself if the provided implementation does not suit your needs
#ifndef ESL_INLINE
#	define ESL_INLINE inline
#endif

#define null NULL

// Sets a breakpoint for use in debuggers.
#define breakpoint asm("int3;")

//
// Some more convenient integer types
//

// [[STDLIB]]
#include <stdint.h>

typedef int8_t s8;
typedef uint8_t u8;

typedef int16_t s16;
typedef uint16_t u16;

typedef int32_t s32;
typedef uint32_t u32;

typedef int64_t s64;
typedef uint64_t u64;

#define U8_MIN  (0)
#define U16_MIN (0)
#define U32_MIN (0)
#define U64_MIN (0)

#define S8_MIN  INT8_MIN
#define S16_MIN INT16_MIN
#define S32_MIN INT32_MIN
#define S64_MIN INT64_MIN

#define U8_MAX  UINT8_MAX
#define U16_MAX UINT16_MAX
#define U32_MAX UINT32_MAX
#define U64_MAX UINT64_MAX

#define S8_MAX  INT8_MAX
#define S16_MAX INT16_MAX
#define S32_MAX INT32_MAX
#define S64_MAX INT64_MAX

// [[STDLIB]]
#include <inttypes.h>

#define U8_FORMAT  "%"PRIu8
#define U16_FORMAT "%"PRIu16
#define U32_FORMAT "%"PRIu32
#define U64_FORMAT "%"PRIu64

#define S8_FORMAT  "%"PRIi8
#define S16_FORMAT "%"PRIi16
#define S32_FORMAT "%"PRIi32
#define S64_FORMAT "%"PRIi64

//
// Functions for dealing with memory
//

void move_bytes(void *destination, const void *source, u64 num_bytes);
void set_bytes(void *destination, u8 byte, u64 num_bytes);
bool bytes_match(const void *a, const void *b, u64 num_bytes);

//
// Sized strings
//

// [[STDLIB]]
#include <string.h>

typedef struct {
	u8 *data;
	u64 length;
	// Fields not mentioned when initializing struct variables with designated initialization
	// (eg. `string foo = { .data = allocate(34), .length = 34 }`) get initialized to 0.
	// We therefore make the default to free the string by naming it 'should_not_free'.
	bool should_not_free;
} string;

u64 length_of_cstr(const char *cstr);
void string_to_cstr(char *cstr, const string s);
char *string_to_cstr_copy(const string s);

string string_from_cstr_copy(const char *cstr);
string string_from_cstr(const char *cstr);
string string_from_byte(u8 byte, u64 size);

void string_destroy(string *s);

void print_string(const string s);

//
// Context
// 'context' can be seen as a struct of parameters anonymously passed to every function.
//

// Takes in a size N and returns a pointer to a memory area of at least N bytes.
typedef void *(*allocator_function)(size_t N);
// Expands or possibly moves the memory area pointed to by pointer P such that
// it becomes at least N bytes large.
// Returns a pointer to the new or expanded memory area.
// Pointer P should not be used as it may have been freed.
// If P is null the function works like allocator_function.
typedef void *(*reallocator_function)(void *P, size_t N);
// Frees the memory area pointed to by pointer P
typedef void (*freer_function)(void *P);

typedef struct {
	// Global allocator, reallocator and freer, used to allocate memory by later functions.
	allocator_function allocator;
	reallocator_function reallocator;
	freer_function freer;
} Context_type;

extern Context_type context;

// TODO: context_push_allocator();
// TODO: context_pop_allocator();
// TODO: context_enable_temp();
// TODO: context_disable_temp();

// Use mfree() to use the freer associated with malloc().
// free() gets overwritten to call to context.freer().
#define mfree(pointer) (free)(pointer)
// Provide mrealloc() as and alias for realloc() for a more consistant naming
// scheme. You can still use realloc().
#define mrealloc(pointer) (realloc)(pointer)

#define allocate(size) context.allocator(size)
#define reallocate(pointer, size) context.reallocator(pointer, size)
#define free(pointer) context.freer(pointer)

//
// Runtime logging
//

// [[STDLIB]]
#include <stdarg.h>

#ifdef ESL_NO_LOGGING
#	define log_info(...)
#	define log_warning(...)
#	define log_error(...)
#else

void __esl_log_info(const char *format, ...);
void __esl_log_warning(const char *format, ...);
void __esl_log_error(const char *format, ...);

void __esl_log_file_info(const char *filename, u64 line);

#	ifdef ESL_LOG_FILE_INFO
#		define log_info(...)    __esl_log_file_info(__FILE__, __LINE__); __esl_log_info(__VA_ARGS__)
#		define log_warning(...) __esl_log_file_info(__FILE__, __LINE__); __esl_log_warning(__VA_ARGS__)
#		define log_error(...)   __esl_log_file_info(__FILE__, __LINE__); __esl_log_error(__VA_ARGS__)
#	else
#		define log_info(...)    __esl_log_info(__VA_ARGS__)
#		define log_warning(...) __esl_log_warning(__VA_ARGS__)
#		define log_error(...)   __esl_log_error(__VA_ARGS__)
#	endif // ESL_LOG_FILE_INFO
#endif // ESL_NO_LOGGING

//
// Dynamic arrays
//

// -- USAGE --
// To get started use define_array_type(name, type) to create a new array type.
// 'name' specifies the name of the new array type.
// 'type' specifies the base type used for the items in the new array type.
//
// For example, to create a new array type with strings, do the following:
//   > define_array_type(String_Array, string);
//
// This creates a new type alias to an anonymous struct with three fields:
//     String_Array.data - a pointer to the place in memory where the data for the array is stored.
//     String_Array.length - the number of occupied slots in the array.
//     String_Array.capacity - the number of total, both occupied and free, slots in the array.
//
// This new type is nothing special, only a structure with some fields.
// To make this new type more useful, use define_array_functions(array_type, function_prefix).
// This creates functions for interacting with the new array type.
// 'array_type' specifies the custom array type to use for these new functions.
// 'function_prefix' specifies the prefix to use for the new functions.
//
// For example, to create functions for interacting with the new custom array
// type String_Array, do the following:
//   > define_array_functions(String_Array, string_array);
//
// This creates a number of new functions. Below you see an example for
// creating, modifying and then destroying a custom u64_Array. Note that the
// example only shows what you *can* do, not what you *should* do.
//
// ```c
// #define ESL_IMPLEMENTATION
// #include "esl.h"
//
// define_array_type(u64_Array, u64);
// define_array_functions(u64_Array, u64_array);
//
// int main(void) {
//     u64_Array foo = {0};
//
//     for (u64 i = 0; i < 16; ++i) {
//         if (i % 2 == 0) {
//             u64_array_append(&foo, i);
//         }
//     }
//
//     for (u64 i = 0; i < foo.length; ++i) {
//         printf(U64_FORMAT" ", foo.data[i]);
//     }
//     printf("\n");
//
//     u64_array_destroy(&foo);
// }
// ```
//
// Output should look something like this:
//   > 0 2 4 6 8 10 12 14 

// Always aligns upwards
// ALIGN_TO_BYTES(0, 8) -> 8
// ALIGN_TO_BYTES(3, 8) -> 8
// ALIGN_TO_BYTES(7, 8) -> 8
// ALIGN_TO_BYTES(8, 8) -> 16 (yes)
#define ALIGN_TO_BYTES(bytes_to_align, align_to_num) (bytes_to_align + align_to_num - (bytes_to_align % align_to_num))

#define define_array_type(name, type) \
typedef struct { \
	type *data; \
	u64 length; \
	u64 capacity; \
} (name); \

// Contents after array.data[array.length-1] may change.
#define __custom_array_expect_capacity_exact(prefix, type) \
/* Symbol declaration */ \
void prefix##_expect_capacity_exact(type *array, u64 expected_capacity); \
ESL_INLINE void prefix##_expect_capacity_exact(type *array, u64 expected_capacity) { \
	if (array->capacity < expected_capacity) { \
		array->data = reallocate(array->data, expected_capacity * sizeof(*array->data)); \
		array->capacity = expected_capacity; \
	} \
}

// Contents after array.data[array.length-1] may change.
#define __custom_array_expect_capacity(prefix, type) \
/* Symbol declaration */ \
void prefix##_expect_capacity(type *array, u64 expected_capacity); \
ESL_INLINE void prefix##_expect_capacity(type *array, u64 expected_capacity) { \
	u64 next_capacity = ALIGN_TO_BYTES(array->capacity, 8) - 8; \
	if (next_capacity == 0) { \
		next_capacity = 8; \
	} \
	while (next_capacity < expected_capacity) next_capacity *= 2; \
	prefix##_expect_capacity_exact(array, next_capacity); \
}

#define __custom_array_new(prefix, type) \
/* Symbol declaration */ \
type prefix##_new(u64 size); \
ESL_INLINE type prefix##_new(u64 size) { \
	type result = {0}; \
	prefix##_expect_capacity(&result, size); \
	return result; \
}

#define __custom_array_new_exact(prefix, type) \
/* Symbol declaration */ \
type prefix##_new_exact(u64 size); \
ESL_INLINE type prefix##_new_exact(u64 size) { \
	type result = {0}; \
	prefix##_expect_capacity_exact(&result, size); \
	return result; \
}

#define __custom_array_expect_available(prefix, type) \
/* Symbol declaration */ \
void prefix##_expect_available(type *array, u64 expected_available_slots); \
ESL_INLINE void prefix##_expect_available(type *array, u64 expected_available_slots) { \
	prefix##_expect_capacity(array, array->length + expected_available_slots); \
}

// Use sparingly
#define __custom_array_append(prefix, type) \
/* Symbol declaration */ \
void prefix##_append(type *array, typeof(*array->data) item); \
ESL_INLINE void prefix##_append(type *array, typeof(*array->data) item) { \
	prefix##_expect_available(array, 1); \
	array->data[array->length] = item; \
	array->length += 1; \
}

// Unordered
#define __custom_array_remove_fast(prefix, type) \
/* Symbol declaration */ \
void prefix##_remove_fast(type *array, u64 index); \
ESL_INLINE void prefix##_remove_fast(type *array, u64 index) { \
	array->data[index] = array->data[array->length - 1]; \
	array->length -= 1; \
}

// Slow
#define __custom_array_remove_ordered(prefix, type) \
/* Symbol declaration */ \
void prefix##_remove_ordered(type *array, u64 index); \
ESL_INLINE void prefix##_remove_ordered(type *array, u64 index) { \
	for (u64 i = 0; i < array->length - index - 1; ++i) { \
		array->data[index + i] = array->data[index + i + 1]; \
	} \
	array->length -= 1; \
}

// Also slow, use sparingly
#define __custom_array_insert(prefix, type) \
/* Symbol declaration */ \
void prefix##_insert(type *array, typeof(*array->data) item, u64 index); \
ESL_INLINE void prefix##_insert(type *array, typeof(*array->data) item, u64 index) { \
	prefix##_expect_available(array, 1); \
	for (u64 i = array->length - 1; i >= index; --i) { \
		array->data[i + 1] = array->data[i]; \
	} \
	array->data[index] = item; \
	array->length += 1; \
}

// Does not handle freeing of items in the array, only the array itself!
// Remember to free them yourself, or the memory leakage demon will have a good time!
// This also goes for *_remove_many()
#define __custom_array_destroy(prefix, type) \
/* Symbol declaration */ \
void prefix##_destroy(type *a); \
ESL_INLINE void prefix##_destroy(type *a) { \
	if (a->data != null) { \
		free(a->data); \
		a->data = null; \
	} \
	a->length = 0; \
	a->capacity = 0; \
}

#define __custom_array_remove_many(prefix, type) \
/* Symbol declaration */ \
void prefix##_remove_many(type *array, u64 index, u64 count); \
ESL_INLINE void prefix##_remove_many(type *array, u64 index, u64 count) { \
	for (u64 i = index + count; i < array->length; ++i) { \
		array->data[i - count] = array->data[i]; \
	} \
	array->length -= count; \
}

#define __custom_array_clone(prefix, type) \
/* Symbol declaration */ \
type prefix##_clone(type input); \
ESL_INLINE type prefix##_clone(type input) { \
	type result = prefix##_new(input.length); \
	memcpy(result.data, input.data, input.length * sizeof(*input.data)); \
	result.length = input.length; \
	return result; \
}

#define define_array_functions(array_type, function_prefix) \
	__custom_array_expect_capacity_exact(function_prefix, array_type); \
	__custom_array_expect_capacity(function_prefix, array_type); \
	__custom_array_expect_available(function_prefix, array_type); \
	__custom_array_new_exact(function_prefix, array_type); \
	__custom_array_new(function_prefix, array_type); \
	__custom_array_destroy(function_prefix, array_type); \
	__custom_array_clone(function_prefix, array_type); \
	__custom_array_append(function_prefix, array_type); \
	__custom_array_remove_fast(function_prefix, array_type); \
	__custom_array_remove_ordered(function_prefix, array_type); \
	__custom_array_insert(function_prefix, array_type); \
	__custom_array_remove_many(function_prefix, array_type);

//
// String builders
//

typedef struct {
	u8 *data;
	u64 length;
	u64 capacity;
} String_Builder;

void string_builder_expect_capacity_exact(String_Builder *builder, u64 expected_capacity);
void string_builder_expect_capacity(String_Builder *builder, u64 expected_capacity);
void string_builder_expect_available(String_Builder *builder, u64 expected_available_slots);

String_Builder string_builder_new_exact(u64 size);
String_Builder string_builder_new(u64 size);

void string_builder_destroy(String_Builder *builder);

String_Builder string_builder_clone(String_Builder input);

void string_builder_append(String_Builder *builder, const string str);
void string_builder_append_cstr(String_Builder *builder, const char *cstr);
// Use sparingly
void string_builder_append_byte(String_Builder *builder, u8 byte);
// Useful for quickly using a builder as a cstr without converting it to something else.
void string_builder_append_null(String_Builder *builder);

// Use sparingly
void string_builder_remove_byte_index(String_Builder *builder, u64 index);
void string_builder_remove_many(String_Builder *string_builder, u64 index, u64 count);

void string_builder_insert(String_Builder *builder, string str, u64 index);
void string_builder_insert_cstr(String_Builder *builder, const char *cstr, u64 index);

string string_builder_to_string(const String_Builder builder);

void print_string_builder(const String_Builder b);
void print_string_builder_line(const String_Builder b);

//
// Functions for dealing with files
//

// [[STDLIB]]
#include <stdio.h>

// Returns -1 on error.
// Logs error.
s64 size_of_file(FILE *file);

// Returns false on error.
// Logs error.
// Sets errno to 0 at the start of the function. You can therefore check if
// (errno != 0) and then strerror(errno) if the function returns false.
bool read_entire_file(const char *filename, String_Builder *builder);

// Truncates the file.
// Returns false on error.
// Logs error.
// Sets errno to 0 at the start of the function. You can therefore check if
// (errno != 0) and then strerror(errno) if the function returns false.
bool write_entire_file(const char *filename, const String_Builder builder);

#endif // ESL_H_

#ifdef ESL_IMPLEMENTATION

// [[STDLIB]]
#include <errno.h>

Context_type context = {
	// Uses the default allocator provided by the standard library by default
	.allocator = malloc,
	.reallocator = realloc,
	.freer = free,
};

//
// Functions for dealing with memory
//

ESL_INLINE void move_bytes(void *destination, const void *source, u64 num_bytes) {
	if (destination == null || source == null) return;
	if (num_bytes == 0) return;

	// memmove() will probably use your system much better than some homemade implementation.
	memmove(destination, source, num_bytes);
}

ESL_INLINE void set_bytes(void *destination, u8 byte, u64 num_bytes) {
	if (destination == null) return;
	if (num_bytes == 0) return;

	// memset() will probably use your system much better than some homemade implementation.
	memset(destination, byte, num_bytes);
}

ESL_INLINE bool bytes_match(const void *a, const void *b, u64 num_bytes) {
	for (u64 i = 0; i < num_bytes; ++i) {
		if (((u8*)a)[i] != ((u8*)b)[i]) {
			return false;
		}
	}

	return true;
}

//
// Sized strings
//

ESL_INLINE u64 length_of_cstr(const char *cstr) {
	if (cstr == null) return 0;
	// strlen() will probably use your system much better than some homemade implementation.
	return strlen(cstr);
}

ESL_INLINE string string_from_cstr_copy(const char *cstr) {
	u64 length = length_of_cstr(cstr);

	string result = {
		.data = allocate(length),
		.length = length,
	};

	memcpy(result.data, (u8*)cstr, length);

	return result;
}

ESL_INLINE string string_from_cstr(const char *cstr) {
	u64 length = length_of_cstr(cstr);

	string result = {
		.data = (u8*)cstr,
		.length = length,
		.should_not_free = true,
	};

	return result;
}

ESL_INLINE string string_from_byte(u8 byte, u64 size) {
	string result = {
		.data = allocate(size),
		.length = size,
	};

	set_bytes(result.data, byte, size);

	return result;
}

ESL_INLINE void string_to_cstr(char *cstr, const string s) {
	move_bytes(cstr, s.data, s.length);
	cstr[s.length] = '\0';
}

ESL_INLINE char *string_to_cstr_copy(const string s) {
	char *cstr = allocate(s.length + 1);

	memcpy(cstr, s.data, s.length);
	cstr[s.length] = '\0';

	return cstr;
}

ESL_INLINE void string_destroy(string *s) {
	if (!s->should_not_free) {
		free(s->data);
		s->data = null;
	}
	s->length = 0;
}

ESL_INLINE void print_string(const string s) {
	printf("%.*s", (int)s.length, s.data);
}

//
// Runtime logging
//

ESL_INLINE void __esl_log_info(const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);

	printf("[INFO]  "); // Two spaces for text alignment
	vprintf(format, arguments);
	printf("\n");

	va_end(arguments);
}

ESL_INLINE void __esl_log_warning(const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);

	printf("[WARN]  "); // Two spaces for text alignment, [WARNING] is too long
	vprintf(format, arguments);
	printf("\n");

	va_end(arguments);
}

ESL_INLINE void __esl_log_error(const char *format, ...) {
	va_list arguments;
	va_start(arguments, format);

	printf("[ERROR] ");
	vprintf(format, arguments);
	printf("\n");

	va_end(arguments);
}

ESL_INLINE void __esl_log_file_info(const char *filename, u64 line) {
	printf("%s:"U64_FORMAT": ", filename, line);
}

//
// String builders
//

// Some, but not all, functions for string builders work the same as with
// arrays, so create them here.
__custom_array_expect_capacity_exact(string_builder, String_Builder);
__custom_array_expect_capacity(string_builder, String_Builder);
__custom_array_expect_available(string_builder, String_Builder);
__custom_array_new_exact(string_builder, String_Builder);
__custom_array_new(string_builder, String_Builder);
__custom_array_destroy(string_builder, String_Builder);
__custom_array_clone(string_builder, String_Builder);
__custom_array_remove_many(string_builder, String_Builder);

ESL_INLINE void string_builder_append(String_Builder *builder, const string str) {
	string_builder_expect_available(builder, str.length);
	memcpy(builder->data + builder->length, str.data, str.length);
	builder->length += str.length;
}

ESL_INLINE void string_builder_append_cstr(String_Builder *builder, const char *cstr) {
	u64 length = length_of_cstr(cstr);
	string_builder_expect_available(builder, length);
	memcpy(builder->data + builder->length, cstr, length);
	builder->length += length;
}

// Use sparingly
ESL_INLINE void string_builder_append_byte(String_Builder *builder, u8 byte) {
	string_builder_expect_available(builder, 1);
	builder->data[builder->length] = byte;
	builder->length += 1;
}

// Useful for quickly using a builder as a cstr without converting it to something else.
ESL_INLINE void string_builder_append_null(String_Builder *builder) {
	string_builder_append_byte(builder, '\0');
}

// Use sparingly
ESL_INLINE void string_builder_remove_byte_index(String_Builder *builder, u64 index) {
	move_bytes(builder->data + index, builder->data + index + 1, builder->length - index);
	builder->length -= 1;
}

ESL_INLINE void string_builder_insert(String_Builder *builder, string str, u64 index) {
	string_builder_expect_available(builder, str.length);
	move_bytes(builder->data + index + str.length, builder->data + index, builder->length - str.length);
	memcpy(builder->data + index, str.data, str.length);
	builder->length += str.length;
}

ESL_INLINE void string_builder_insert_cstr(String_Builder *builder, const char *cstr, u64 index) {
	u64 length = length_of_cstr(cstr);
	string_builder_expect_available(builder, length);
	move_bytes(builder->data + index + length, builder->data + index, builder->length - index);
	memcpy(builder->data + index, cstr, length);
	builder->length += length;
}

ESL_INLINE string string_builder_to_string(const String_Builder builder) {
	string result = {
		.data = allocate(builder.length),
		.length = builder.length,
	};

	memcpy(result.data, builder.data, builder.length);

	return result;
}

// TODO: rewrite and fix these
ESL_INLINE void print_string_builder(const String_Builder b) {
	printf("%.*s", (int)b.length, b.data);
}
ESL_INLINE void print_string_builder_line(const String_Builder b) {
	printf("%.*s\n", (int)b.length, b.data);
}

//
// Functions for dealing with files
//

// Returns -1 on error.
// Logs error.
s64 size_of_file(FILE *file) {
	if (file == null) {
		log_error("%s(): Expected a valid (FILE *)file, got null pointer.", __func__);
		return -1;
	}

	s64 previous_position = ftell(file);
	if (previous_position < 0) {
		log_error("%s(): Could not ftell(file): %s.", __func__, strerror(errno));
		return -1;
	}

	s32 seek_result = fseek(file, 0, SEEK_END);
	if (seek_result < 0) {
		log_error("%s(): Could not seek to end of file: %s.", __func__, strerror(errno));
		return -1;
	}

	s64 file_size = ftell(file);
	if (file_size < 0) {
		log_error("%s(): Could not get the size of file: %s.", __func__, strerror(errno));
		return -1;
	}

	seek_result = fseek(file, previous_position, SEEK_SET);
	if (seek_result < 0) {
		log_error("%s(): Could not seek to original position in file: %s.", __func__, strerror(errno));
		return -1;
	}

	return file_size;
}

// Returns false on error.
// Logs error.
// Sets errno to 0 at the start of the function. You can therefore check if
// (errno != 0) and then strerror(errno) if the function returns false.
bool read_entire_file(const char *filename, String_Builder *builder) {
	errno = 0;

	if (filename == null) {
		log_error("%s(): Expected a valid (const char *)filename, got null pointer.", __func__);
		return false;
	}

	if (*filename == '\0') {
		log_error("%s(): Cannot read empty filename.", __func__);
		return false;
	}

	if (builder == null) {
		log_error("%s(): Expected a valid String_Builder, got null pointer.", __func__);
		return false;
	}

	FILE *file = fopen(filename, "r");
	if (file == null) {
		log_error("%s(): Could not open file '%s' for reading: %s.", __func__, filename, strerror(errno));
		return false;
	}

	s64 file_size = size_of_file(file);
	if (file_size < 0) {
		// More information is always better, right?
		log_info("%s(): Tried to get the size of file '%s'.", __func__, filename);
		fclose(file);
		return false;
	}

	string_builder_expect_available(builder, file_size);
	// Pointer offset
	fread(builder->data + builder->length, file_size, 1, file);
	if (ferror(file) != 0) {
		log_error("%s(): An error occurred while reading "S64_FORMAT" bytes from file '%s'.", __func__, file_size, filename);
		fclose(file);
		return false;
	}
	builder->length += file_size;

	fclose(file);
	return true;
}

// Truncates the file.
// Returns false on error.
// Logs error.
// Sets errno to 0 at the start of the function. You can therefore check if
// (errno != 0) and then strerror(errno) if the function returns false.
bool write_entire_file(const char *filename, const String_Builder builder) {
	errno = 0;

	if (filename == null) {
		log_error("%s(): Expected a valid (const char *)filename, got null pointer.", __func__);
		return false;
	}

	if (*filename == '\0') {
		log_error("%s(): Cannot read empty filename.", __func__);
		return false;
	}

	if (builder.length == 0) {
		log_error("%s(): Tried to write 0 bytes to file '%s'.", __func__, filename);
		return false;
	}

	FILE *file = fopen(filename, "w");
	if (file == null) {
		log_error("%s(): Could not open file '%s' for reading: %s.", __func__, filename, strerror(errno));
		return false;
	}

	size_t written = fwrite(builder.data, 1, builder.length, file);
	if (written == 0) {
		log_error("%s(): Could not write "U64_FORMAT" bytes to file '%s'.", __func__, builder.length, filename);
		fclose(file);
		return false;
	} else if (builder.length != written) {
		log_info("%s(): Tried to write "U64_FORMAT" bytes to file '%s', but could only write "U64_FORMAT" bytes.", __func__, builder.length, filename, written);
	}

	fclose(file);
	return true;
}

#endif // ESL_IMPLEMENTATION

/* MIT License
 Copyright (c) 2024 deparia

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
*/

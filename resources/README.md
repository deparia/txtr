# Resourcesources

## Fonts

### inconsolata.regular.ttf
Gathered from [1001fonts.com](https://www.1001fonts.com/inconsolata-font.html).
Free for personal and commercial use. (Licensed under the [SIL Open Font License](https://openfontlicense.org/))


# txtr

Text reader in C.

## Getting started
Make sure you have SDL2 installed before continuing.

```console
$ make release
$ ./txtr foofile
```

## Developing
This project uses [esl.h](https://codeberg.org/deparia/esl) for ease of development.
Some information does not get logged in release. See `log_info()` and its definition.

```console
$ make
$ ./txtr foofile
```

in=main.c
headers=esl.h
out=txtr
libs=-lSDL2 -lSDL2_ttf
dev_flags=-Wall -Wextra -g -Og
release_flags=-O3 -s -DRELEASE

$(out): $(in) $(headers)
	cc -o $(out) $(dev_flags) $(in) $(libs)

release: $(in) $(headers)
	cc -o $(out) $(release_flags) $(in) $(libs)
